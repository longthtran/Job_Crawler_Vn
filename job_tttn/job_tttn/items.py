# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Field

class JobItem(scrapy.Item):
    url_job = Field()#
    job_name = Field()#
    job_type = Field()
    comp_name = Field()#
    address = Field()#
    salary = Field()#
    exp = Field()#
    degree = Field()#
    location = Field()#
    genre = Field()#
    sex = Field()
    description = Field()#
    requirement = Field()#
    benefit = Field()#
    time_update = Field()
    time_expire = Field()#
    #scrape_from_url = Field()
    image_url = Field()
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
