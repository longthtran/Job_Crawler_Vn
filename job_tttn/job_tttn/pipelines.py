# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy import log

class MongoDBPipeline(object):
    def __init__(self, collection_name):
        client = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = client[settings['MONGODB_DB']]
        self.collection = db[collection_name]
         
    
    def process_item(self, item, spider):
        #     valid = False
        # if not item['salary']:
        #     valid = False
        #     raise DropItem("Missing salary in job %s!" % item['job_name'])
        #     # for data in item['salary']:
        #     #     if not data:
        #     #         valid = False
        #     #         raise DropItem("Missing salary in job %s!" % item['job_name'])
        # for data in item:
        #     if not data:
        #         valid = False
        #         raise DropItem("Missing {0}!".format(data))
        # if item['job_type'] == 'Bán Hàng':
        #     valid = True
        # if valid:
        if type(item['salary']) is list:
            if len(item['salary']) > 2:
                raise DropItem("Missing salary in job %s!" % item['job_name'])
            elif len(item['salary']) == 1:
                salary = MongoDBPipeline.convert_money(item['salary'][0])
                item['salary'] = salary
            elif len(item['salary']) == 2:
                item['salary'][0] = MongoDBPipeline.convert_money(item['salary'][0])
                item['salary'][1] = MongoDBPipeline.convert_money(item['salary'][1])
        
        self.collection.insert(dict(item))
        log.msg("Job added to collection!",
            level=log.DEBUG, spider=spider)
        return item

    """
    a function change money from string to int
    """
    @staticmethod
    def convert_money(money):
        length = len(money)
        value = 0
        if length > 8 & length < 12: # money from x,000,000 to yyy,000,000
            count = 0
            while length - 1 - 8 - count >= 0:
                value = value + int(money[length - 1 - 8 - count]) * (10 ** (count + 6))
                count = count + 1
        else: # money is USD, ex: 500 2500 31000 (length of money is short)
            value = int(money)
        return value


class ITPipeline(MongoDBPipeline):
    def __init__(self):
        MongoDBPipeline.__init__(self, 'it')
        print "IT Pipeline is initialized and store items to collection it" 

class AUPipeline(MongoDBPipeline):
    def __init__(self):
        MongoDBPipeline.__init__(self, 'au')
        print "AU Pipeline is initialized and store items to collection au"

class SAPipeline(MongoDBPipeline):
    def __init__(self):
        MongoDBPipeline.__init__(self, 'sa')
        print "SA Pipeline is initialized and store items to collection sa"