import scrapy
from job_tttn.items import JobItem
from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess
from job_tttn import settings as my_settings
class MyWorkSpider(scrapy.Spider):
    
    def __init__(self, job_type):
        self.job_type = job_type

    def parse(self, response):
        for job in response.css('a.title'):
            getJob = JobItem()
            url_unicode = job.css('::attr(href)').extract_first()
            url = url_unicode.encode('utf-8')
            getJob['url_job'] = 'https://mywork.com.vn' + url
            #getJob['scrape_from_url'] = response.url
            request = scrapy.Request(url='https://mywork.com.vn'+url, callback=self.parse_job)
            request.meta['getJob'] = getJob
            yield request
        
        next_page = response.xpath('//a[@class="gotoPage"][last()]/@href').extract_first()
        if next_page is not None:
            yield scrapy.Request(url='https://mywork.com.vn' + next_page, callback=self.parse)

    def parse_job(self, response):
        job = response.meta['getJob']
        job['job_name'] = response.css('h1::text')[0].extract()
        job['job_type'] = self.job_type
        image_url = response.xpath('//*[@id="mywork"]/div[3]/div/div/div[1]/div/div[1]/div[1]/div[1]/img/@src').extract_first().encode('utf-8')
        if image_url == 'https://secure1.vncdn.vn/data/images/logo/0.gif':
            image_url = 'default'
        job['image_url'] = image_url
        time_update = response.xpath('//*[@id="mywork"]/div[3]/div/div/div[2]/div[2]/div[1]/div[3]/div[2]').re('\d+-\d+-\d+')
        job['time_update'] = time_update[0]
        job['comp_name'] = response.css('h1.comp-name a::text').extract_first()
        job['location'] = response.xpath('//*[@id="mywork"]/div[3]/div/div/div[1]/div/div[1]/div[1]/div[2]/p[1]/b/span/a/text()').extract_first()
        job['address'] = response.css('tbody tr')[1].css('td::text')[2].extract().strip()
        job['time_expire'] = response.css('tbody tr')[3].css('td::text')[2].extract().strip()
        job['genre'] = response.xpath('//*[@id="mywork"]/div[3]/div/div/div[1]/div/div[1]/div[1]/div[2]/p[2]/b/text()').extract_first()
        # May trang moi de tien VND, may trang cu de USD!!!
        salary = filter(lambda x: x!='', response.xpath('//*[@id="mywork"]/div[3]/div/div/div[1]/div/div[1]/div[1]/div[2]/p[3]/b')
            .re('(\d+,000,000)|(\d+00)'))

        if not salary:
            # return a string not list
            salary = response.xpath('//*[@id="mywork"]/div[3]/div/div/div[1]/div/div[1]/div[1]/div[2]/p[3]/b/text()').extract_first().strip()
        # elif len(salary) == 1:
        #     salary[0] = convert_money(salary[0])
        #     salary =  salary[0]
        # elif len(salary) == 2:
        #     for s in salary:
        #         s = convert_money(s)
        job['salary'] = salary # make sure that list salary is not empty
        
        exp = filter(lambda x: x!="",response.css('li.home-col12')[0].css('p::text').re('(\d+)|(Kh.*)|(Ch.*)'))
        job['exp'] = exp[0]
        #job['exp'] = response.css('li.home-col12')[0].css('p::text').extract_first()
        job['degree'] = response.css('li.home-col123 p::text').extract_first()
        """[description, benefit, requirement] = map(lambda x: response.xpath('//div[@class="desjob-company"]')[x], range(1,4))
        #return list
        benefit = filter(lambda x: x!="", map(lambda x: x.strip(), benefit.xpath('.//text()').extract()))
        description = map(lambda x: x.strip(), description.xpath('.//text()').extract())
        requirement = filter(lambda x: x!="", map(lambda x: x.strip(), requirement.xpath('.//text()').extract()))
        #concat to be a paragraph
        job['description'] = reduce(lambda x,y: x+y, description, '') 
        job['benefit'] = benefit
        job['requirement'] = requirement"""
        if len(response.xpath('//div[@class="desjob-company"]')) > 5: # page moi
            description = response.xpath('//div[@class="desjob-company"][1]/descendant::text()[not(ancestor::header)]').extract()
            benefit = response.xpath('//div[@class="desjob-company"][2]/descendant::text()[not(ancestor::header)]').extract()
            job['benefit'] = filter(lambda x: x!= '', map(lambda x: x.strip(), benefit))
            requirement = response.xpath('//div[@class="desjob-company"][3]/descendant::text()[not(ancestor::header)]').extract()
            
        else: #page cu khong co benefit
            description = response.xpath('////div[@class="desjob-company"][1]/text()').extract()
            benefit = response.xpath('//*[@id="mywork"]/div[3]/div/div/div[1]/div/div[1]/div[2]/div[5]/div/text()').extract()
            requirement = response.xpath('//div[@class="desjob-company"][2]/descendant::text()[not(ancestor::header)]').extract()
            job['benefit'] = benefit
        job['description'] = filter(lambda x: x!= '', map(lambda x: x.strip(), description))
        job['requirement'] = filter(lambda x: x!= '', map(lambda x: x.strip(), requirement))
        yield job
   

class ITSpider(MyWorkSpider):
    name = 'myworkit'
    start_urls = [
        'https://mywork.com.vn/tuyen-dung/38/it-phan-mem.html'
    ]
    custom_settings = {
        'ITEM_PIPELINES' : {
            'job_tttn.pipelines.ITPipeline' : 300
        },
        'CONCURRENT_ITEMS' : 30,
        'CONCURRENT_REQUESTS' : 8
    }
    def __init__(self, job_type='Software'):
        self.job_type = job_type

class AuditSpider(MyWorkSpider):
    name = 'myworkau'
    start_urls = [
        'https://mywork.com.vn/tuyen-dung/6/ke-toan-kiem-toan.html'
    ]
    custom_settings = {
        'ITEM_PIPELINES' : {
            'job_tttn.pipelines.AUPipeline' : 400
        }
    }
    def __init__(self, job_type='Audit'):
        self.job_type = job_type

class SaleSpider(MyWorkSpider):
    name = "myworksa"
    start_urls = [
        'https://mywork.com.vn/tuyen-dung/31/ban-hang.html'
    ]
    custom_settings = {
        'ITEM_PIPELINES' : {
            'job_tttn.pipelines.SAPipeline' : 200
        }
    }
    def __init__(self, job_type='Sale'):
        self.job_type = job_type

# crawler_settings = Settings()
# crawler_settings.setmodule(my_settings)
# process = CrawlerProcess(settings=crawler_settings)

# process.crawl(ITSpider)
# process.crawl(SaleSpider)
# process.crawl(AuditSpider)

# process.start()