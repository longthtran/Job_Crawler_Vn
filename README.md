## Configuration
1. Install scrapy: pip -install scrapy
    
## Run spider
1. cd to your root "scrapyproject" and open Terminal.
    For example, here is the directory tree:
```
    -job_tttn
        -scrapy.cfg
        -job_tttn (Open terminal here !!)
            -items.py
            -middlewares.py
            -pipelines.py
            -settings.py
            -__init__.py
        -spiders
            -__init__.py
            -MyWork.py
```
2. Open 1 terminal and type: ```mongod```
3. Open 1 terminal and type: ```mongo``` to operate on MongoDB.
4. scrapy crawl \<spidername\> <br>
    spidername such as: myworkit, myworkau, myworksa,...

## Export json
There are 2 ways:
1. In your OS command type:
    mongoexport --db \<yourdbname\> --collection \<yourcollectionname\> --out \<yourfilename\>.json

2. scrapy crawl \<spidername\> -o \<yourfilename\>.json (Please note that this way only return output of new data you'are crawling, NOT the entire data in your Database) 